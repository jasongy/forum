var express = require('express');
var router = express.Router();

var moment = require('moment');

var Post = require("../models/postModel");
var PostMessage = require("../models/postMessageModel");
var User = require("../models/userModel");

var session = require('client-sessions');

function requireLogin(req, res, next) {
    if (!req.session.user) {
        res.redirect('/forum/login');
    } else {
        next();
    }
}

router.use(function(req, res, next) {
    if (req.session && req.session.user) {
        User.findOne({username: req.session.user.username }, function (err, user) {
            if (user) {
              req.user = user;
              delete req.user.password;
              req.session.user = user;
              res.locals.user = user;
            }
            next();
        });
    } else {
        next();
    }
});

router.use(session({
    cookieName: 'session',
    secret: 'blahhhhhf',
    duration: 30 * 60 * 1000,
    activeDuration: 5 * 60 * 1000,
    httpOnly: true,
    secure: true,
    ephemeral: true}));

router.get('/login', function(req, res, next) {
    if (!req.session.user) {            
        res.render('login/login');
    } else {
        res.redirect("/forum");
    }
});

router.get('/logout', requireLogin, function(req, res) {
    req.session.reset();
    res.redirect("/forum/login");
});

router.post('/login', function(req, res, next) {
    User.findOne({username: req.body.username}, function (err, user) {
        if (!user) {
            res.send("Invalid login information provided.");
        } else {
            if (req.body.password === user.password) {
                req.user = user;
                delete req.user.password;
                req.session.user = user;
                res.redirect("/forum");
            } else {
                res.send("Login failed");
            }
        }
    });
});

router.post('/register', function(req, res, next) {
    var newUser = new User(req.body);
    newUser.save(function (err) {
        if (err) console.log(err);
        req.session.user = newUser;
        res.redirect("/forum");
    });
});

router.get('/', requireLogin, function(req, res, next) {
    Post.find({deleted: false}, function (err, posts) {
        if (err) throw err;
        for (i = 0; i < posts.length; i++) {
            posts[i].fdate = moment(posts[i].createdAt).format("MMMM Do, YYYY");
        }
        console.log(req.session.user);
        res.render('forum/main', {posts: posts, user: req.session.user});
    });
});

router.get('/newThread', requireLogin, function(req, res, next) {
    res.render("forum/newThread");
});

router.post('/newThread', requireLogin, function(req, res, next) {
    var newPost = new Post({title: req.body.title, user: req.session.user});
    // Need to add user object from cookie later
    newPost.save(function(err) {
        if (err) throw err;
    });
    var newPostMessage = new PostMessage({message: req.body.message, user: req.session.user,
        _post: newPost});
    newPostMessage.save(function(err) {
        if (err) throw err;
        res.redirect("/forum");
    });
});

function addFormattedDates(messages) {
    for (i = 0; i < messages.length; i++) {
        messages[i].fdate = moment(messages[i].createdAt).format("MMMM Do YYYY h:mm a");
    }
    return messages;
}

router.get('/:id', requireLogin, function(req, res, next) {
    var currentPost;
    Post.findById(req.params.id, function(err, post) {
        if (err) throw err;
        currentPost = post;
    });
    PostMessage.find({_post: req.params.id, deleted: false}, function(err, messages) {
        if (err) throw err;
        var thread = {post: currentPost, messages: addFormattedDates(messages), user: req.session.user};
        res.render("forum/thread", thread);
   });
});

router.post('/:id', requireLogin, function(req, res, next) {
    Post.findById(req.params.id, function(err, post) {
        var newPostMessage = new PostMessage({message: req.body.message, user: req.session.user, _post: post});
        newPostMessage.save(function(err) {
            if (err) throw err;
            res.redirect("/forum/" + req.params.id);
        });
    });
});

router.get("/deletePost/:id", requireLogin, function(req, res, next) {
    Post.findByIdAndUpdate(req.params.id, {deleted: true}, function(err) {
        if (err) throw err;
        res.redirect("/forum");
    });
});

// Deletes by PostMessage ID
router.get("/delete/:id", requireLogin, function(req, res, next) {
    PostMessage.findByIdAndUpdate(req.params.id, {deleted: true}, function (err, message) {
        if (err) throw err;
        Post.findById(message._post, function (err, post) {
            if (err) throw err;
            PostMessage.find({_post: post._id, deleted: false}, function (err, messages) {
                var thread = {post: post, messages: addFormattedDates(messages), user: req.session.user};
                res.render("forum/thread", thread);
            });
        });
    });
});

router.get("/edit/:id", requireLogin, function(req, res, next) {
    PostMessage.findById(req.params.id, function(err, message) {
        if (err) throw err;
        res.render("forum/editPost", {message});
    });
});

router.post("/edit/:id", requireLogin, function(req, res, next) {
    req.body.createdAt = Date.now();
    PostMessage.findByIdAndUpdate(req.params.id, req.body, function(err, message) {
        if (err) throw err;
        res.redirect("/forum/" + message._post);
    });
});

// Things to do:
// - Boolean added for soft deletes
// - Should not display any documents that have been soft-deleted

module.exports = router;