/**
 * Created by jason on 5/2/16.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var User = mongoose.model("user", {
    username: String,
    password: String,
    name: String
});

module.exports = User;