/**
 * Created by jason on 5/2/16.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Post = mongoose.model('post',
    {
        title: String,
        createdAt: { type: Date, default: Date.now, index: true },
        user: Object,
        deleted: {type: Boolean, default: false}
    });

module.exports = Post;