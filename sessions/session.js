/**
 * Created by jason on 5/3/16.
 */

var session = require('client-sessions');

var sessionMiddleware = session({
        cookieName: 'session',
        secret: 'blahhhhhf',
        duration: 30 * 60 * 1000,
        activeDuration: 5 * 60 * 1000,
        httpOnly: true,
        secure: true,
        ephemeral: true});

function sessionHandler(req, res, next) {
    sessionMiddleware(req, res, next);
}



module.exports = sessionHandler;